#!/usr/bin/env ruby

Node = Struct.new(:prev, :value, :next)

class LinkedList
attr_reader :head

	def initialize(value=nil)
		@head = Node.new(nil, value, nil)
	end

	def push_back value
		insert(size, value)
	end

	def push_front value
		insert(0, value)
	end

	def pop_back
		current = @head
		for i in 1..size-1
			current = current[:next]
		end
		aux = current[:next][:value]
		erase(size)
		return aux
	end

	def pop_front
		aux = @head[:value]
		erase(0)
		return aux
	end

	def insert(position, *values)
		current = @head
		if position == 0
			for value in values.reverse
				@head = Node.new(nil, value, @head)
			end
		else
			for i in 1..position-1
				current = current[:next]
			end
			for value in values.reverse
				current[:next] = Node.new(current, value, current[:next])
			end
		end
		self
	end

	def erase(position)
		if position > size
			nil
		end
		if position == 0
			if @head[:next].nil?
				@head = nil
			else
				@head = @head[:next]
			end
		else
			current = @head
			for i in 1..position-1
				current = current[:next]
			end
			current[:next] = (current[:next])[:next]
		end
	end	

	def size
		if @head == nil
			0
		end
		counter = 1
		current = @head
		until current[:next] == nil
			counter+=1
			current=current[:next]
		end
		counter
	end

	def [](position)
			if position > size
				return nil
			end
			current = @head
			for i in 1..position-1
				current = current[:next]
			end
			return current[:next][:value]
	end

	def to_s
		current = @head
		to_s_array = []
		until current[:next] == nil
			to_s_array.push(current[:value])
			current=current[:next]
		end
		to_s_array.push(current[:value])
		p "[#{to_s_array.join(', ')}]"
	end

end
