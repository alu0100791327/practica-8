require 'spec_helper'

describe Bibliogem do
	
	describe Referencia do
		it 'tiene un numero de version' do
			expect(Bibliogem::VERSION).not_to be nil
		end

		it "puede crear referencias" do
			expect{Referencia.new(["Pedro de los Palotes"],"La mitica gema de la bibliografia dorada","Las gemas doradas","Publicaciones ULL", 3, "02-10-2015",["12345678910"], ["1234567890123"])}.not_to raise_error
		end

		it "debe tener uno o mas autores" do
			expect{Referencia.new([],"","","",0,"01-01-2000",[],[])}.to raise_error
		end

		it "debe tener un titulo" do
			expect{Referencia.new([""],nil,"","",0,"01-01-2000",[0],[0])}.to raise_error
		end

		it "debe tener o no una serie" do
			expect{Referencia.new([""],"",nil,"",0,"01-01-2000",[0],[0])}.not_to raise_error
		end

		it "debe tener una editorial" do
			expect{Referencia.new([""],nil,"","",0,"01-01-2000",[0],[0])}.to raise_error
		end

		it "debe tener un numero de edicion" do
			expect{Referencia.new([""],"","","",nil,"01-01-2000",[0],[0])}.to raise_error
		end

		it "debe tener una fecha de publicacion" do
			expect{Referencia.new([""],"","","",0, nil,[0],[0])}.to raise_error
		end

		it "debe tener uno o mas numeros ISBN" do
			expect{Referencia.new([""],"","","",0, nil,[],[])}.to raise_error
		end

		ObjetoPrueba = Referencia.new(["Pedro de los Palotes"],"La mitica gema de la bibliografia dorada","Las gemas doradas","Publicaciones ULL", 3, "02-10-2015",[12345678910], [1234567890123])

		it "debe tener un metodo para obtener el listado de autores" do
			expect(ObjetoPrueba.autores).to eq(["Pedro de los Palotes"])
		end

		it "debe tener un metodo para obtener el titulo" do
			expect(ObjetoPrueba.titulo).to eq("La mitica gema de la bibliografia dorada")
		end

		it "debe tener un metodo para obtener la serie" do
			expect(ObjetoPrueba.serie).to eq("Las gemas doradas")
		end

		it "debe tener un metodo para obtener la editorial" do
			expect(ObjetoPrueba.editorial).to eq("Publicaciones ULL")
		end

		it "debe tener un metodo para obtener el numero de edicion" do
			expect(ObjetoPrueba.nedicion).to eq(3)
		end

		it "debe tener un metodo para obtener fecha de publicacion" do
			expect(ObjetoPrueba.fecha).to eq(Date::strptime("02-10-2015", "%d-%m-%Y"))
		end

		it "debe tener un metodo para obtener el listado de ISBN" do
			expect(ObjetoPrueba.isbn10).to eq([12345678910])
		end

		describe ReferenciaLibro do
			it "es una clase derivada de Referencia" do
				ReferenciaLibro.new(["Pedro de los Palotes"],"La mitica gema de la bibliografia dorada","Las gemas doradas","Publicaciones ULL", 3, "02-10-2015",[12345678910], [1234567890123]).should be_kind_of(Referencia)
			end
		end

		describe ReferenciaPeriodica do
			it "es una clase derivada de Referencia" do
				ReferenciaPeriodica.new(["Pedro de los Palotes"],"La mitica gema de la bibliografia dorada","Las gemas doradas","Publicaciones ULL", 3, "02-10-2015",[12345678910], [1234567890123]).should be_kind_of(Referencia)
			end
			describe ReferenciaPeriodicaRevista do
				it "es una clase derivada de ReferenciaPeriodica" do
					ReferenciaPeriodicaRevista.new(["Pedro de los Palotes"],"La mitica gema de la bibliografia dorada","Las gemas doradas","Publicaciones ULL", 3, "02-10-2015",[12345678910], [1234567890123]).should be_kind_of(ReferenciaPeriodica)
				end
			end
			describe ReferenciaPeriodicaDiario do
				it "es una clase derivada de ReferenciaPeriodica" do
					ReferenciaPeriodicaDiario.new(["Pedro de los Palotes"],"La mitica gema de la bibliografia dorada","Las gemas doradas","Publicaciones ULL", 3, "02-10-2015",[12345678910], [1234567890123]).should be_kind_of(ReferenciaPeriodica)
				end
			end
			describe ReferenciaPeriodicaElectronico do
				it "es una clase derivada de ReferenciaPeriodica" do
					ReferenciaPeriodicaElectronico.new(["Pedro de los Palotes"],"La mitica gema de la bibliografia dorada","Las gemas doradas","Publicaciones ULL", 3, "02-10-2015",[12345678910], [1234567890123]).should be_kind_of(ReferenciaPeriodica)
				end
			end
		end

	end
# autores, titulo, serie, editorial, nedicion, fecha, isbn10, isbn13
	describe Node do	
		it "Debe existir un nodo de la lista con sus datos, su siguiente y su anterior" do
			prueba = (Node.new("Anterior","Valor", "Siguiente"))
			prueba[:value].should eql "Valor"
			prueba[:next].should eql "Siguiente"
			prueba[:prev].should eql "Anterior"
		end
	end

	describe LinkedList do
		let(:lprueba) {LinkedList.new(0)}

		it "Extrae el primer elemento de la lista" do
			expect(lprueba.pop_front).to eq(0)
		end

		it "Puede insertar un elemento" do
			lprueba.insert(0, 2)
			expect(lprueba.to_s).to eq("[2, 0]")
		end

		it "Puede insertar varios elementos" do
			lprueba.insert(1,2,3)
			expect(lprueba.to_s).to eq("[0, 2, 3]")
		end

		it "Debe existir una lista con su cabeza" do
			expect(lprueba.head).to be_an_instance_of Node
		end
	end
end
